package stugregistrering.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RentalagreementVO
{
	@XmlElement
	public String cabin;
	@XmlElement
	public String username;
	@XmlElement
	public String useremail;
	@XmlElement
	public String phonenumber;

	public RentalagreementVO(String cabin, String username, String useremail, String phonenumber)
	{
		super();
		this.cabin = cabin;
		this.username = username;
		this.useremail = useremail;
		this.phonenumber = phonenumber;
	}

	public RentalagreementVO()
	{
		super();
	}

	public String getCabin()
	{
		return cabin;
	}

	public void setCabin(String cabin)
	{
		this.cabin = cabin;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getUseremail()
	{
		return useremail;
	}

	public void setUseremail(String useremail)
	{
		this.useremail = useremail;
	}

	public String getPhonenumber()
	{
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber)
	{
		this.phonenumber = phonenumber;
	}

}
