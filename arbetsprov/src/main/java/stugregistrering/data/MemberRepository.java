/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package stugregistrering.data;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import stugregistrering.model.Rentalagreement;

@ApplicationScoped
public class MemberRepository
{

	@Inject
	private EntityManager em;

	public Rentalagreement findReservationsById(Long id)
	{
		return em.find(Rentalagreement.class, id);
	}

	public List<Rentalagreement> findAllReservationsByName()
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Rentalagreement> criteria = cb.createQuery(Rentalagreement.class);
		Root<Rentalagreement> reservation = criteria.from(Rentalagreement.class);

		criteria.select(reservation).orderBy(cb.asc(reservation.get("id")));
		return em.createQuery(criteria).getResultList();
	}

	public List<Rentalagreement> findAllReservationsByCabin()
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Rentalagreement> criteria = cb.createQuery(Rentalagreement.class);
		Root<Rentalagreement> reservation = criteria.from(Rentalagreement.class);

		criteria.select(reservation).orderBy(cb.asc(reservation.get("cabin")));
		return em.createQuery(criteria).getResultList();
	}

}
