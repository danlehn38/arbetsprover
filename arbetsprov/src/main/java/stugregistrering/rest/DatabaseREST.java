package stugregistrering.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import stugregistrering.data.MemberRepository;
import stugregistrering.model.Rentalagreement;
import stugregistrering.model.RentalagreementVO;
import stugregistrering.service.Registration;

@Path("/database")
@RequestScoped
public class DatabaseREST
{

	public DatabaseREST()
	{

	}

	@Inject
	private MemberRepository repository;

	@Inject
	Registration registration;

	@GET
	@Path("/getReservations")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Rentalagreement> listAllMembers()
	{
		return repository.findAllReservationsByName();
	}

	@POST
	@Path("/addReservations")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteChampionFromIDList(RentalagreementVO rentalagreementVO)
	{
		Rentalagreement rentalagreement = new Rentalagreement();
		rentalagreement.setCabin(rentalagreementVO.getCabin());
		rentalagreement.setName(rentalagreementVO.getUsername());
		rentalagreement.setEmail(rentalagreementVO.getUseremail());
		rentalagreement.setPhonenumber(rentalagreementVO.getPhonenumber());

		try
		{
			registration.addRegistration(rentalagreement);
		} catch (Exception e)
		{
			return Response.serverError().build();
		}

		return Response.ok(rentalagreement).build();
	}
}
